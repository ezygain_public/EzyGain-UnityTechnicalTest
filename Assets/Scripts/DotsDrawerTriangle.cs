#region : using
    using UnityEngine;
    using EzyGain.UIElements;
#endregion

namespace EzyGain
{
    public class DotsDrawerTriangle : MonoBehaviour
    {
        #region : SERIALIZED FIELD
            /// <Summary>
            /// Triangle base is always parallel to the screen horizontal edges, triangle is always isosceles
            ///
            /// Triangle contains properties:
            /// - positions: table of Vector2 containing the three coordinates of the triangle vertexes
            /// positions[0].x gives the horizontal coordinate of the first vertex, positions[0].y gives the vertical coordinate
            ///
            /// You have access to the dots through the table 'dots_'
            /// dot_[0].position.x gives the horizontal coordinate of the first dot, dot_[0].position.y gives the vertical coordinate
            /// </Summary>
            [SerializeField] UIElements.Triangle triangle;
            [SerializeField] Dot[] dots_;
        #endregion

        public void DrawDots()
        {
            //Write your code here only.
            //Edit the dots coordinates in dots_ table so that the shape fits into the triangle (see given pictures triangle_*)
        }
    }
}