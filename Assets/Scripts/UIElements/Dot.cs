#region : using
    using UnityEngine;
    using UnityEngine.UI;
#endregion

namespace EzyGain.UIElements
{
    [RequireComponent(typeof(Image))]
    public class Dot : MonoBehaviour
    {
        #region : PROPERTIES
            /// <summary>
            /// Dot position x,y
            /// </summary>
            public Vector2 position
            {
                get
                {
                    return position_;
                }
                set
                {
                    position_ = value;
                    rectTransform.anchoredPosition = position_;
                }
            }

            /// <summary>
            /// Dot color
            /// </summary>
            public Color color
            {
                get
                {
                    return color_;
                }
                set
                {
                    color_ = value;
                    image.color = color_;
                }
            }
        #endregion

        #region : PARAMETERS
            private Vector2 position_;
            [SerializeField] private Color color_;
            private RectTransform rectTransform;
            private Image image;
        #endregion

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            image = GetComponent<Image>();
            image.color = color;

            position_ = rectTransform.anchoredPosition;
        }
    }
}