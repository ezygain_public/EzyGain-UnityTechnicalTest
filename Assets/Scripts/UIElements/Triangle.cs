#region : using
    using UnityEngine;
    using UnityEngine.Events;
#endregion

namespace EzyGain.UIElements
{
    public class Triangle : MonoBehaviour
    {
        #region : PROPERTIES
            public Vector2[] positions
            {
                get
                {
                    return positions_;
                }
                set
                {
                    positions_ = value;
                    UpdateRenderers();
                }
            }
        #endregion

        #region : SERIALIZED FIELDS
            [Tooltip("In this order: left, right, top")]
            [SerializeField] RectTransform[] vertexes;
            [SerializeField] protected UnityEvent onNewDrawing;
        #endregion

        #region : PARAMETERS
            //triangle
            Vector2[] positions_;

            //locals
            int height_;
            int width_;
            bool up_;

            //environment size
            protected int  screenSizeX;
            protected int screenSizeY;
        #endregion

        void Awake()
        {
            RectTransform mainCanvasTransform = null;
            Canvas[] canvases = FindObjectsOfType<Canvas>();
            foreach(Canvas canvas in canvases)
            {
                if(canvas.isRootCanvas)
                {
                    mainCanvasTransform = canvas.GetComponent<RectTransform>();
                    break;
                }
            }
            if(mainCanvasTransform == null)
                throw(new System.NotSupportedException("RootCanvas couldn't be found!"));

            screenSizeX = (int) mainCanvasTransform.sizeDelta.x;
            screenSizeY = (int) mainCanvasTransform.sizeDelta.y;

            positions_ = new Vector2[3];
        }

        void OnEnable()
        {
            NewDrawing();
        }

        void UpdateRenderers()
        {
            vertexes[0].anchoredPosition = positions[0];
            vertexes[1].anchoredPosition = positions[1];
            vertexes[2].anchoredPosition = positions[2];
        }

        public void NewDrawing()
        {
            width_ = Random.Range((int)(0.1f*screenSizeX),(int)(0.5f*screenSizeX));
            height_ = Random.Range((int)(0.1f*screenSizeY),(int)(0.5f*screenSizeY));
            up_ = Random.Range(0,2) == 0;

            if(up_)
            {
                positions_[0] = new Vector2(Random.Range(0,screenSizeX-width_),Random.Range(0,screenSizeY-height_));
            }
            else
            {
                positions_[0] = new Vector2(Random.Range(0,screenSizeX-width_),Random.Range(height_,screenSizeY));
            }
            positions_[1] = new Vector2(positions[0].x+width_,positions[0].y);
            if(up_)
            {
                positions_[2] = new Vector2((positions[0].x+positions[1].x)/2,positions[0].y+height_);
            }
            else
            {
                positions_[2] = new Vector2((positions[0].x+positions[1].x)/2,positions[0].y-height_);
            }
            
            UpdateRenderers();
            onNewDrawing.Invoke();
        }
    }
}