#region : using
    using UnityEngine;
    using UnityEngine.Events;
#endregion

namespace EzyGain.UIElements
{
    public class Rectangle : MonoBehaviour
    {
        #region : PROPERTIES
            public Vector2 position
            {
                get
                {
                    return position_;
                }
                set
                {
                    position_ = value;
                    UpdateRenderers();
                }
            }

            public int width
            {
                get
                {
                    return width_;
                }
                set
                {
                    width_ = value;
                    UpdateRenderers();
                }
            }

            public int height
            {
                get
                {
                    return height_;
                }
                set
                {
                    height_ = value;
                    UpdateRenderers();
                }
            }
        #endregion

        #region : SERIALIZED FIELDS
            [Tooltip("In this order: bottomLeft, bottomRight, topRight, topLeft")]
            [SerializeField] RectTransform[] vertexes;
            [SerializeField] protected UnityEvent onNewDrawing;
        #endregion

        #region : PARAMETERS
            //rectangle
            Vector2 position_;
            int width_;
            int height_;

            //environment size
            protected int  screenSizeX;
            protected int screenSizeY;
        #endregion

        void Awake()
        {
            RectTransform mainCanvasTransform = null;
            Canvas[] canvases = FindObjectsOfType<Canvas>();
            foreach(Canvas canvas in canvases)
            {
                if(canvas.isRootCanvas)
                {
                    mainCanvasTransform = canvas.GetComponent<RectTransform>();
                    break;
                }
            }
            if(mainCanvasTransform == null)
                throw(new System.NotSupportedException("RootCanvas couldn't be found!"));

            screenSizeX = (int) mainCanvasTransform.sizeDelta.x;
            screenSizeY = (int) mainCanvasTransform.sizeDelta.y;
        }

        void OnEnable()
        {
            NewDrawing();
        }
        
        void UpdateRenderers()
        {
            vertexes[0].anchoredPosition = position;
            vertexes[1].anchoredPosition = position + Vector2.right*width;
            vertexes[2].anchoredPosition = vertexes[1].anchoredPosition + Vector2.up*height;
            vertexes[3].anchoredPosition = position + Vector2.up*height;
        }

        public void NewDrawing()
        {
            width_ = Random.Range((int)(0.1f*screenSizeX),(int)(0.5f*screenSizeX));
            height_ = Random.Range((int)(0.1f*screenSizeY),(int)(0.5f*screenSizeY));
            position_ = new Vector2(Random.Range(0,screenSizeX-width),Random.Range(0,screenSizeY-height));
            
            UpdateRenderers();
            onNewDrawing.Invoke();
        }
    }
}