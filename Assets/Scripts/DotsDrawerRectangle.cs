#region : using
    using UnityEngine;
    using EzyGain.UIElements;
#endregion

namespace EzyGain
{
    public class DotsDrawerRectangle : MonoBehaviour
    {
        #region : SERIALIZED FIELDS
            /// <Summary>
            /// Rectangle edges are always parallel to the screen edges.
            /// Rectangle contains properties:
            /// - position: (Vector2) indicating position of bottom left vertex
            /// position.x gives the horizontal coordinate of the vertex, position.y gives the vertical coordinate
            /// - width: (int) width of the rectangle
            /// - height: (int) height of the rectangle
            ///
            /// You have access to the dots through the table 'dots_'
            /// </Summary>
            [SerializeField] UIElements.Rectangle rectangle;
        
            [SerializeField] Dot[] dots_;
        #endregion

        public void DrawDots()
        {
            //Write your code here only.
            //Edit the dots coordinates in dots_ table so that the shape fits into the rectangle (see given pictures rectangle_* and rectangle_preserveAspectRatio_*)
        }
    }
}