# EzyGain UnityTechnicalTest

## Installation

* Install Unity 2020.3.24f1
* You will need to create a personal account to get the free license access.

## Development

* To test your code just press the play button above the scene view in the Unity editor. Press it again to exit the play mode.
* Your code changes gets implemented automatically while you are not in play mode.